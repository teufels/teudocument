<?php declare(strict_types=1);

namespace TeuDocument\Subscriber;

use Shopware\Core\Content\Product\Events\ProductListingCriteriaEvent;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\ContainsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\MultiFilter;
use Shopware\Core\Framework\Struct\ArrayStruct;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use Shopware\Storefront\Page\PageLoadedEvent;
use Shopware\Storefront\Page\Product\ProductPageLoadedEvent;
use Shopware\Storefront\Page\Search\SearchPageLoadedEvent;
use Shopware\Storefront\Page\Suggest\SuggestPageLoadedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use TeuDocument\Core\Content\Document\DocumentEntity;
use TeuDocument\Service\MediaHandleService;


class SearchSubscriber implements EventSubscriberInterface
{
    final const SUGGEST_LIMIT     = 10;

    public function __construct(
        private readonly SystemConfigService $systemConfigService,
        private readonly EntityRepository $teuDocumentRepository,
        private readonly MediaHandleService $mediaHandleService
    )
    {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            SearchPageLoadedEvent::class        => 'doSearch',
            SuggestPageLoadedEvent::class       => 'doSearchSuggest',
        ];
    }

    public function doSearch(PageLoadedEvent $event)
    {
        if (!$this->systemConfigService->get('TeuDocument.config.activeDocSearch', $event->getSalesChannelContext()->getSalesChannelId())) {
            return;
        }

        $query = $event->getPage()->getSearchTerm();
        //var_dump($query);die;
        $docResults = $this->docSearch($query, $event->getSalesChannelContext());
        if ($this->systemConfigService->get('TeuDocument.config.showPreviewOnSearchPage', $event->getSalesChannelContext()->getSalesChannelId())) {
            foreach ($docResults->getElements() as $doc) {
                $this->checkPreview($doc);
            }
        }
        $results['docs'] = [
            'label' => 'TeuDocument.search.doc',
            'data'  => $docResults->getElements(),
            'total' => $docResults->getTotal()
        ];

        $event->getPage()->assign(['teuSearch' => $results]);
    }

    public function doSearchSuggest(SuggestPageLoadedEvent $event)
    {
        if (!$this->systemConfigService->get('TeuDocument.config.activeDocSearch', $event->getSalesChannelContext()->getSalesChannelId())) {
            return;
        }
        $query = $event->getPage()->getSearchTerm();

        $docResults = $this->docSearch($query, $event->getSalesChannelContext(), true);
        if ($this->systemConfigService->get('TeuDocument.config.showPreviewOnSearchSuggest', $event->getSalesChannelContext()->getSalesChannelId())) {
            foreach ($docResults->getElements() as $doc) {
                $this->checkPreview($doc);
            }
        }
        $results['docs'] = [
            'label' => 'TeuDocument.search.doc',
            'data'  => $docResults->getElements(),
            'total' => $docResults->getTotal()
        ];

        $event->getPage()->assign(['teuSearch' => $results]);
    }

    private function checkPreview(&$doc) {
            if ($doc->getTranslated()['previewId']) {
                //continue;
            } elseif (!isset($doc->getTranslated()['mediaId'])) {
                //continue;
            } elseif ($doc->isExternal) {
                //continue;
            } else {
                $tranlated = $doc->getTranslated();
                $tranlated['previewId'] =  $this->mediaHandleService->checkPreviewForMedia([$doc->getTranslated()['mediaId']]);
                $doc->setTranslated($tranlated);
            }
    }

    private function docSearch(string $query, SalesChannelContext $salesChannelContext, bool $isSuggest = false) {

        $docs = $this->getDocs($query, $salesChannelContext, $isSuggest);

        return $docs;

    }

    private function getDocs($query, SalesChannelContext $salesChannelContext, bool $isSuggest = false)
    {
        $query = trim((string) $query);
        $words = explode(' ', $query);
        if (count($words) > 0) {
            $criteria = new Criteria();
            if($isSuggest) {
                $criteria->setLimit(static::SUGGEST_LIMIT);
                $criteria->setTotalCountMode(Criteria::TOTAL_COUNT_MODE_EXACT);
            }
            $criteria->addAssociation('media');
            $criteria->addAssociation('tags');
            $filter = [];
            foreach ($words as $word) {
                $filter[] = new ContainsFilter('name', $word);
                $filter[] = new ContainsFilter('tags.name', $word);
                //$filter[] = new ContainsFilter('media.filename', $word);
            }
            $criteria->addFilter(new MultiFilter(MultiFilter::CONNECTION_OR, $filter));

            return $result = $this->teuDocumentRepository->search($criteria, $salesChannelContext->getContext());
        }
        return null;
    }
}
