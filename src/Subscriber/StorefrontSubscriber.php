<?php declare(strict_types=1);

namespace TeuDocument\Subscriber;

use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Content\Product\ProductEvents;
use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\Event\DataMappingEvent;
use Shopware\Core\Framework\Struct\ArrayStruct;
use Shopware\Core\Framework\Struct\StructCollection;
use Shopware\Core\System\SalesChannel\Entity\SalesChannelRepository;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use Shopware\Core\Framework\DataAbstractionLayer\Event\EntityLoadedEvent;
use Shopware\Storefront\Page\Product\ProductPageLoadedEvent;
use Shopware\Storefront\Page\Product\QuickView\MinimalQuickViewPageLoadedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Shopware\Core\Content\Category\CategoryEntity;
use TeuDocument\Core\Content\Document\DocumentEntity;
use Shopware\Core\Content\Media\MediaEntity;
use TeuDocument\Service\MediaHandleService;

class StorefrontSubscriber implements EventSubscriberInterface
{

    public function __construct(
        private readonly SystemConfigService $systemConfigService,
        private readonly MediaHandleService $mediaHandleService,
        private readonly EntityRepository $mediaRepository,
        private readonly EntityRepository $teuDocumentRepository,
        private readonly SalesChannelRepository $productRepository)
    {

    }

    public static function getSubscribedEvents(): array
    {
        return [
            ProductPageLoadedEvent::class => 'onProductPageLoaded',
        ];
    }

    public function onProductPageLoaded(ProductPageLoadedEvent $event): void
    {

        if (!$this->systemConfigService->get('TeuDocument.config.activeDetailDoc')) {
            return;
        }

        $product = $event->getPage()->getProduct();


        $productDocsIds = (isset($product->getTranslated()['customFields']['teu_cus_prod_documents']) ? $product->getTranslated()['customFields']['teu_cus_prod_documents'] : isset($product->getCustomFields()['teu_cus_prod_documents']) ?? null);

        $criteria = new Criteria();
        $criteria->addAssociation('tags');
        $productDocsExt = [];

        if ($productDocsIds) {
            foreach ($productDocsIds as $id) {

                /** @var DocumentEntity $teuProductDocument */
                $productDoc = $this->teuDocumentRepository->search($criteria, $event->getContext())->get($id);
                if ($this->systemConfigService->get('TeuDocument.config.showPreviewOnDetail')) {
                    //print_r($productDoc);die;
                    if ($productDoc->getTranslated()['previewId']) {
                        //continue;
                    } elseif ($productDoc->isExternal) {
                        //continue;
                    } else {
                        $tranlated = $productDoc->getTranslated();
                        $tranlated['previewId'] = $this->mediaHandleService->checkPreviewForMedia([$productDoc->getTranslated()['mediaId']]);
                        $productDoc->setTranslated($tranlated);
                    }
                }
                array_push($productDocsExt, $productDoc);
            }

            $product->addExtension('documents',
                new StructCollection($productDocsExt)
            );
        }

    }

}
