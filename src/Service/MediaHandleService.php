<?php declare(strict_types=1);

namespace TeuDocument\Service;

use Shopware\Core\Content\Media\File\FileSaver;
use Shopware\Core\Content\Media\File\MediaFile;
use Shopware\Core\Content\Media\MediaService;
use Shopware\Core\Framework\Context;
use Shopware\Core\Content\Media\Exception\DuplicatedMediaFileNameException;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\Uuid\Uuid;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use Spatie\PdfToImage\Pdf;

class MediaHandleService
{
    const TEMP_NAME = 'image-import-from-url';      //prefix for temporary files, downloaded from URL
    const MEDIA_FOLDER = 'Document Preview (auto generated)';                 //name of the folder in Shopware's media data structure
    const AUTOGENRATE_EXT= 'jpg';
    const AUTOGENRATE_TMP_FOLDER= '/tmp';


    public function __construct (
        private readonly SystemConfigService $systemConfigService,
        private readonly EntityRepository $mediaRepository,
        private readonly EntityRepository $mediaFolderRepository,
        private readonly MediaService $mediaService,
        private readonly FileSaver $fileSaver
    )
    {
    }

    public function checkPreviewForMedia($id)
    {

        $folder= $this->checkPreviewFolder();
        $media = $this->mediaRepository->search((new Criteria($id)),Context::createDefaultContext())->first();
        if ($media->getMediaType()->getName() == 'IMAGE') {
            return $media->getId();
        } else {
            $existMedia = $this->mediaRepository->search((new Criteria())->addFilter(new EqualsFilter('fileName', $media->getFileName()))->addFilter(new EqualsFilter('fileExtension', self::AUTOGENRATE_EXT)),Context::createDefaultContext())->first();
            if ($existMedia) {
                return $existMedia->getId();
            }

            if ($media->getFileExtension() != 'pdf') {
                return;
            }
            return $this->genratePreviewFromFile($media->getUrl(),$media->getFileName(),$folder);
        }


    }

    public function checkPreviewFolder()
    {
        $existFolderID = $this->mediaFolderRepository->searchIds((new Criteria())->addFilter(new EqualsFilter('name', self::MEDIA_FOLDER)),Context::createDefaultContext())->firstId();
        if (!$existFolderID) {
            $existFolderID = $this->mediaFolderRepository->create(
                [
                    [
                        'name' => self::MEDIA_FOLDER,
                        'configuration' => [
                            'id' => Uuid::randomHex()
                        ]
                    ]
                ],
                Context::createDefaultContext()
            );
        }

        return $existFolderID;
    }

    public function genratePreviewFromFile($url,$filename,$folder)
    {
        if(!is_dir($_SERVER['DOCUMENT_ROOT'].self::AUTOGENRATE_TMP_FOLDER)) {
            mkdir($_SERVER['DOCUMENT_ROOT'].self::AUTOGENRATE_TMP_FOLDER);
        }
        $orgfile = new Pdf($_SERVER['DOCUMENT_ROOT'].parse_url($url, PHP_URL_PATH));
        $orgfile->setCompressionQuality(80)->width(500)->saveImage($_SERVER['DOCUMENT_ROOT'].self::AUTOGENRATE_TMP_FOLDER.'/'.$filename.'.jpg');
        $mediaId = $this->addPreviewToMediaFromFile($filename,$_SERVER['DOCUMENT_ROOT'].self::AUTOGENRATE_TMP_FOLDER.'/'.$filename.'.jpg',Context::createDefaultContext(),$folder);
        unlink($_SERVER['DOCUMENT_ROOT'].self::AUTOGENRATE_TMP_FOLDER.'/'.$filename.'.jpg');
        return $mediaId;
    }


    public function addPreviewToMediaFromFile (string $fileName, string $filePath, Context $context, $folder =  self::MEDIA_FOLDER)
    {
        //compose the path to file

        if (file_exists($filePath)) {
            //get the file extension
            $fileExtension = self::AUTOGENRATE_EXT;

            //create media record from the image and return its ID
            return $this->createMediaFromFile($filePath,$fileName, $fileExtension, $context, $folder);
        } else {
            return NULL;
        }
    }


    /**
     * Method, that creates a new media record from a local file and returns its ID
     *
     * @param string $filePath
     * @param string $fileName
     * @param string $fileExtension
     * @param Context $context
     * @return string|null
     */
    private function createMediaFromFile (string $filePath, string $fileName,string $fileExtension, Context $context,$mediaFolder = self::MEDIA_FOLDER)
    {
        $mediaId = null;

        //get additional info on the file
        $fileSize = filesize($filePath);
        $mimeType = mime_content_type($filePath);


        $existMedia = $this->mediaRepository->search((new Criteria())->addFilter(new EqualsFilter('fileName', $fileName))->addFilter(new EqualsFilter('fileExtension', $fileExtension)),Context::createDefaultContext());

        if ($existMedia->count()) {
            return $existMedia->first()->getId();
        }
        //create and save new media file to the Shopware's media library
        try {
            $mediaFile = new MediaFile($filePath, $mimeType, $fileExtension, $fileSize);
            if ($mediaFolder == self::MEDIA_FOLDER) {
                $mediaId = $this->mediaService->createMediaInFolder($mediaFolder, $context, false);
            } else {
                $mediaId = $this->createMediaInOwnFolder($mediaFolder, $context, false);
            }

            $this->fileSaver->persistFileToMedia(
                $mediaFile,
                $fileName,
                $mediaId,
                $context
            );
        }
        catch (DuplicatedMediaFileNameException $e) {
            echo($e->getMessage());
            $mediaId = $this->mediaCleanup($mediaId, $context);
        }
        catch (\Exception $e) {
            echo($e->getMessage());
            $mediaId = $this->mediaCleanup($mediaId, $context);
        }

        return $mediaId;
    }

    /**
     * Method, that takes care of deleting the newly created media record, if something goes wrong with saving data to it
     *
     * @param string $mediaId
     * @param Context $context
     * @return null
     */
    private function mediaCleanup (string $mediaId, Context $context)
    {
        $this->mediaRepository->delete([['id' => $mediaId]], $context);
        return null;
    }

    public function createMediaInOwnFolder($folder, Context $context, bool $private = true): string
    {
        $mediaId = Uuid::randomHex();
        $this->mediaRepository->create(
            [
                [
                    'id' => $mediaId,
                    'private' => $private,
                    'mediaFolderId' => $folder,
                ],
            ],
            $context
        );

        return $mediaId;
    }
}


