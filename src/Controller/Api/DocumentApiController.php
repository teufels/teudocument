<?php declare(strict_types=1);

namespace TeuDocument\Controller\Api;

use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\MultiFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\NotFilter;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Shopware\Core\Framework\Routing\Annotation\RouteScope;
use TeuDocument\Core\Content\Document\DocumentEntity;
use TeuDocument\Service\MediaHandleService;

#[Route(defaults: ['_routeScope' => ['api']])]
class DocumentApiController extends AbstractController
{

    public function __construct(
        private readonly SystemConfigService $systemConfigService,
        private readonly MediaHandleService $mediaHandleService,
        private readonly EntityRepository $teuDocumentRepository,
    )
    {
    }

    #[Route(path: '/api/teu-document/generate-preview', name: 'api.action.teudocument.generate-preview', methods: ['GET','POST'])]
    public function generatePreview(Request $request, Context $context): JsonResponse
    {
        $criteria = new Criteria();
        $criteria->addAssociation('translations.media');
        $criteria->addFilter(new MultiFilter(MultiFilter::CONNECTION_AND, [
            new EqualsFilter('previewId', null),
            new EqualsFilter('translations.media.fileExtension', 'pdf'),
            new NotFilter(MultiFilter::CONNECTION_AND, [new EqualsFilter('mediaId', null)] )
        ]));

        $documents = $this->teuDocumentRepository->search($criteria, $context);
        if (!$documents) {
            return new JsonResponse(['success' => true, 'info' => 'noFiles']);
        }
        foreach ($documents as $document) {
            foreach ($document->get('translations') as $translation) {
                $this->mediaHandleService->checkPreviewForMedia([$translation->get('mediaId')]);
            }
        }

        return new JsonResponse(['success' => true, 'info' => 'generate']);
    }
}