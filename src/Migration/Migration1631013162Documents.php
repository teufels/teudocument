<?php declare(strict_types=1);

namespace TeuDocument\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1631013162Documents extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1_631_013_162;
    }

    public function update(Connection $connection): void
    {
        $connection->exec('
            CREATE TABLE IF NOT EXISTS `teu_product_document` (
            `id` BINARY(16) NOT NULL,
            `document_test_id` BINARY(16) NULL,
            `created_at` DATETIME(3) NOT NULL,
            `updated_at` DATETIME(3) NULL,
            PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

            CREATE TABLE IF NOT EXISTS `teu_product_document_translation` (
            `teu_product_document_id` BINARY(16) NOT NULL,
            `language_id` BINARY(16) NOT NULL,
            `name` VARCHAR(255) NOT NULL,
            `media_id` BINARY(16) NULL,
            `created_at` DATETIME(3) NOT NULL,
            `updated_at` DATETIME(3) NULL,
            PRIMARY KEY (`teu_product_document_id`, `language_id`),
            CONSTRAINT `fk.teu_product_document_translation.teu_product_document_id` FOREIGN KEY (`teu_product_document_id`)
                REFERENCES `teu_product_document` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
            CONSTRAINT `fk.teu_product_document_translation.language_id` FOREIGN KEY (`language_id`)
                REFERENCES `language` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
             CONSTRAINT `fk.teu_product_document_translation.media_id` FOREIGN KEY (`media_id`)
                REFERENCES `media` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

            CREATE TABLE IF NOT EXISTS `teu_product_document_tag` (
              `teu_product_document_id` BINARY(16) NOT NULL,
              `tag_id` BINARY(16) NOT NULL,
              PRIMARY KEY (`teu_product_document_id`, `tag_id`),
              CONSTRAINT `fk.teu_product_document_tag.teu_product_document_id` FOREIGN KEY (`teu_product_document_id`)
                REFERENCES `teu_product_document` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
              CONSTRAINT `fk.teu_product_document_tag.tag_id` FOREIGN KEY (`tag_id`)
                REFERENCES `tag` (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
        ');
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }
}
