<?php declare(strict_types=1);

namespace TeuDocument\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1693563885DocumentCategoryMapping extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1693563885;
    }

    public function update(Connection $connection): void
    {
        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS `teu_product_document_document_category` (
                `teu_document_id` BINARY(16) NOT NULL,
                `teu_document_category_id` BINARY(16) NOT NULL,
                PRIMARY KEY (`teu_document_id`,`teu_document_category_id`),
                KEY `fk.teu_document_document_category.teu_document_id` (`teu_document_id`),
                KEY `fk.teu_document_document_category.teu_document_category_id` (`teu_document_category_id`),
                CONSTRAINT `fk.teu_document_document_category.teu_document_id` FOREIGN KEY (`teu_document_id`) REFERENCES `teu_product_document` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                CONSTRAINT `fk.teu_document_document_category.teu_document_category_id` FOREIGN KEY (`teu_document_category_id`) REFERENCES `teu_product_document_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
SQL;
        $connection->executeStatement($sql);
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }
}
