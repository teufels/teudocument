<?php declare(strict_types=1);

namespace TeuDocument\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1693490556AddDescription extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1693490556;
    }

    public function update(Connection $connection): void
    {
        $sql = <<<SQL
ALTER TABLE `teu_product_document_translation`
        ADD COLUMN IF NOT EXISTS  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL;
SQL;
        $connection->executeStatement($sql);
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }
}
