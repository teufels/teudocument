<?php declare(strict_types=1);

namespace TeuDocument\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1651754203ExternalLink extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1_651_754_203;
    }

    public function update(Connection $connection): void
    {
        $sql = <<<SQL
ALTER TABLE `teu_product_document`
        ADD COLUMN IF NOT EXISTS `is_external` TINYINT(1) DEFAULT 0,
        ADD COLUMN IF NOT EXISTS `external_new_tab` TINYINT(1) DEFAULT 0;
ALTER TABLE `teu_product_document_translation`
        ADD COLUMN IF NOT EXISTS `external_link` VARCHAR(255) DEFAULT NULL;
SQL;
        $connection->executeStatement($sql);
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }
}
