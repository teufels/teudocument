<?php declare(strict_types=1);

namespace TeuDocument\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1694001695AddCustomFields extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1694001695;
    }

    public function update(Connection $connection): void
    {
        $sql = <<<SQL
ALTER TABLE `teu_product_document_translation`
        ADD COLUMN IF NOT EXISTS `custom_fields` JSON NULL,
        ADD CONSTRAINT `json.teu_product_document_translation.custom_fields`
                 CHECK (JSON_VALID(`custom_fields`));
ALTER TABLE `teu_product_document_category_translation`
        ADD COLUMN IF NOT EXISTS `custom_fields` JSON NULL,
        ADD CONSTRAINT `json.teu_product_document_category_translation.custom_fields`
                 CHECK (JSON_VALID(`custom_fields`));
SQL;
        $connection->executeStatement($sql);

    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }
}
