<?php declare(strict_types=1);

namespace TeuDocument\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1690548400AddDocumentCategories extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1690548400;
    }

    public function update(Connection $connection): void
    {
        $sql = <<<SQL
ALTER TABLE `teu_product_document`
        ADD COLUMN IF NOT EXISTS  `category_ids` JSON NULL;
ALTER TABLE `teu_product_document`
        DROP CONSTRAINT IF EXISTS  `json.teu_product_document.category_ids`;
ALTER TABLE `teu_product_document`
        ADD CONSTRAINT `json.teu_product_document.category_ids` CHECK  (JSON_VALID(`category_ids`));

CREATE TABLE IF NOT EXISTS `teu_product_document_category` (
    `id` BINARY(16) NOT NULL,
    `active` TINYINT(1) DEFAULT 1,
    `parent_id` BINARY(16) NULL,
    `after_category_id` BINARY(16) NULL,
    `path` LONGTEXT NULL,
    `level` INT(11) unsigned NOT NULL DEFAULT 1,
    `child_count` INT(11) unsigned NOT NULL DEFAULT 0,
    
    `created_at` DATETIME(3) NOT NULL,
    `updated_at` DATETIME(3) NULL,
    PRIMARY KEY (`id`),
     CONSTRAINT `fk.teu_product_document_category.parent_id` FOREIGN KEY (`parent_id`)
                REFERENCES `teu_product_document_category` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
    CONSTRAINT `fk.teu_product_document_category.after_category_id` FOREIGN KEY (`after_category_id`)
                REFERENCES `teu_product_document_category` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8mb4
    COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `teu_product_document_category_translation` (
    `teu_product_document_category_id` BINARY(16) NOT NULL,
    `language_id` BINARY(16) NOT NULL,
    
    `name` varchar(255),
    `description` TEXT NULL,
    
    `created_at` DATETIME(3) NOT NULL,
    `updated_at` DATETIME(3) NULL,
    
    PRIMARY KEY (`teu_product_document_category_id`, `language_id`),
      CONSTRAINT `fk.teu_product_document_category_translation.language_id` FOREIGN KEY (`language_id`)
        REFERENCES `language` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
      CONSTRAINT `fk.teu_product_document_category_translation.category_id` FOREIGN KEY (`teu_product_document_category_id`)
        REFERENCES `teu_product_document_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8mb4
    COLLATE = utf8mb4_unicode_ci;

SQL;

        $connection->exec($sql);
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }
}
