<?php declare(strict_types=1);

namespace TeuDocument\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1690362224AddPreviewImage extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1690362224;
    }

    public function update(Connection $connection): void
    {
        $sql = <<<SQL
ALTER TABLE `teu_product_document_translation`
        ADD COLUMN IF NOT EXISTS  `preview_id` BINARY(16) NULL;
ALTER TABLE `teu_product_document_translation`
        ADD CONSTRAINT `fk.teu_product_document_translation.preview_id` FOREIGN KEY IF NOT EXISTS (`preview_id`)
                REFERENCES `media` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;
SQL;
        $connection->executeStatement($sql);
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }
}
