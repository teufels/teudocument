<?php declare(strict_types=1);

namespace TeuDocument\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1700582652addCategoryBreadcrumb extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1700582652;
    }

    public function update(Connection $connection): void
    {
        $connection->executeStatement('ALTER TABLE `teu_product_document_category_translation` ADD `breadcrumb` json NULL AFTER `name`;');
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }
}
