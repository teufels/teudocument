<?php declare(strict_types=1);

namespace TeuDocument;

use Shopware\Core\System\CustomField\CustomFieldTypes;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\Plugin;
use Shopware\Core\Framework\Plugin\Context\InstallContext;
use Shopware\Core\Framework\Uuid\Uuid;
use TeuDocument\Service\MediaHandleService;
use Shopware\Core\Framework\Plugin\Context\UpdateContext;

class TeuDocument extends Plugin
{
    public function install(InstallContext $context): void
    {
        $this->checkCustomFields($context);
    }

    public function update(UpdateContext $context): void
    {
        $this->checkCustomFields($context);
    }

    private function checkCustomFields($context)
    {
        $customCatFields = [

            [
                'id' => Uuid::fromBytesToHex('teuCusDocument00'),
                'name' => 'teu_custom_prod_document_field_set',
                'active' => true,
                'config' => [
                    'label' => [
                        'en-GB' => 'Documents',
                        'de-DE' => 'Dokumente'
                    ],
                ],
                'customFields' => [
                    [
                        'id' => Uuid::fromBytesToHex('teuCusDocumentS0'),
                        'name' => 'teu_cus_prod_documents',
                        'type' => CustomFieldTypes::SELECT,
                        'config' => [
                            'label' => [
                                'en-GB' => 'Documents',
                                'de-DE' => 'Dokumente',
                            ],
                            'entity' => 'teu_product_document',
                            'componentName' => 'sw-entity-multi-id-select',

                            'customFieldType' => CustomFieldTypes::ENTITY,
                        ]
                    ],
                ],
                'relations' => [
                    [
                        'id' => Uuid::fromBytesToHex('teuCusDocumentS0'),
                        'entityName' => 'product'
                    ]
                ]
            ]

        ];

        /** @var EntityRepository $repo */
        $repo = $this->container->get('custom_field_set.repository');

        foreach ($customCatFields as $customCatFieldSet) {
            $repo->upsert([$customCatFieldSet], $context->getContext());
        }
    }
}
