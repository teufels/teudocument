<?php declare(strict_types=1);

namespace TeuDocument\Core\Content\DocumentCategory;

use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

/**
 * @method void              add(DocumentCategoryEntity $entity)
 * @method void              set(string $key, DocumentCategoryEntity $entity)
 * @method DocumentCategoryEntity[]    getIterator()
 * @method DocumentCategoryEntity[]    getElements()
 * @method DocumentCategoryEntity|null get(string $key)
 * @method DocumentCategoryEntity|null first()
 * @method DocumentCategoryEntity|null last()
 */
class DocumentCategoryCollection extends EntityCollection
{
    protected function getExpectedClass(): string
    {
        return DocumentCategoryEntity::class;
    }
}
