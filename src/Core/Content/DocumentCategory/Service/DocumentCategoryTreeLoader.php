<?php declare(strict_types=1);

namespace TeuDocument\Core\Content\DocumentCategory\Service;

use Shopware\Core\Framework\DataAbstractionLayer\Util\AfterSort;
use TeuDocument\Core\Content\DocumentCategory\DocumentCategoryCollection;
use TeuDocument\Core\Content\DocumentCategory\DocumentCategoryEntity;
use TeuDocument\Core\Content\DocumentCategory\Tree\DocumentCategoryTree;
use TeuDocument\Core\Content\DocumentCategory\Tree\DocumentCategoryTreeItem;

class DocumentCategoryTreeLoader
{
    private readonly DocumentCategoryTreeItem $treeItem;

    public function __construct()
    {
        $this->treeItem = new DocumentCategoryTreeItem(null, []);
    }

    public function getTree(?string $rootId, DocumentCategoryCollection $categories, ?DocumentCategoryEntity $active): DocumentCategoryTree
    {
        $parents = [];
        $items = [];
        foreach ($categories as $category) {
            $item = clone $this->treeItem;
            $item->setCategory($category);

            $parents[$category->getParentId()][$category->getId()] = $item;
            $items[$category->getId()] = $item;
        }

        foreach ($parents as $parentId => $children) {
            if (empty($parentId)) {
                continue;
            }

            $sorted = AfterSort::sort($children);

            $filtered = \array_filter($sorted, static fn (DocumentCategoryTreeItem $filter) => $filter->getCategory()->getActive());

            if (!isset($items[$parentId])) {
                continue;
            }

            $item = $items[$parentId];
            $item->setChildren($filtered);
        }

        $root = $parents[$rootId] ?? $parents;
        $root = AfterSort::sort($root);

        $filtered = [];
        /** @var DocumentCategoryTreeItem $item */
        foreach ($root as $key => $item) {
            if (!$item->getCategory()->getActive()) {
                continue;
            }

            $filtered[$key] = $item;
        }

        return new DocumentCategoryTree($active, $filtered);
    }
}