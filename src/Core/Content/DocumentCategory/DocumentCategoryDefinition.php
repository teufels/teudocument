<?php declare(strict_types=1);

namespace TeuDocument\Core\Content\DocumentCategory;

use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\BoolField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ChildCountField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ChildrenAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\CreatedAtField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\ApiAware;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\PrimaryKey;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\WriteProtected;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IdField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ManyToManyAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\OneToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ParentAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ParentFkField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\StringField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\TranslatedField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\TranslationsAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\TreeLevelField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\TreePathField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\UpdatedAtField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ManyToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\FkField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\JsonField;
use TeuDocument\Core\Content\Document\Aggregate\DocumentCategoryMappingDefinition;
use TeuDocument\Core\Content\Document\DocumentDefinition;
use TeuDocument\Core\Content\DocumentCategory\Aggregate\DocumentCategoryTranslation\DocumentCategoryTranslationDefinition;

class DocumentCategoryDefinition extends EntityDefinition
{
    final public const ENTITY_NAME = 'teu_product_document_category';

    public function getEntityName(): string
    {
        return self::ENTITY_NAME;
    }

    public function getCollectionClass(): string
    {
        return DocumentCategoryCollection::class;
    }

    public function getEntityClass(): string
    {
        return DocumentCategoryEntity::class;
    }

    protected function defineFields(): FieldCollection
    {
        return new FieldCollection([
            (new IdField('id', 'id'))->addFlags(new PrimaryKey(), new Required()),
            //(new IdField('parent_id', 'parentId'))->addFlags(),
            new ParentFkField(self::class),
            //(new IdField('after_category_id', 'afterCategoryId'))->addFlags(),
            new FkField('after_category_id', 'afterCategoryId', self::class),
            (new BoolField('active','active')),
            (new TreeLevelField('level', 'level')),
            (new TreePathField('path', 'path')),
            (new ChildCountField()),
            (new TranslationsAssociationField(
                DocumentCategoryTranslationDefinition::class,
                'teu_product_document_category_id'
            ))->addFlags(new Required()),

            new ParentAssociationField(self::class, 'id'),
            new ChildrenAssociationField(self::class),

            (new TranslatedField('name'))->addFlags(new Required()),
            (new TranslatedField('breadcrumb'))->addFlags(new ApiAware(), new WriteProtected()),
            (new TranslatedField('description')),
            (new CreatedAtField()),
            (new UpdatedAtField()),

            new TranslatedField('customFields'),

            new ManyToManyAssociationField('documents', DocumentDefinition::class, DocumentCategoryMappingDefinition::class, 'teu_document_category_id', 'teu_document_id'),

        ]);
    }
}
