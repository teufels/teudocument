<?php declare(strict_types=1);

namespace TeuDocument\Core\Content\DocumentCategory\Aggregate\DocumentCategoryTranslation;

use Enqueue\Dbal\JSON;
use Shopware\Core\Framework\DataAbstractionLayer\EntityCustomFieldsTrait;
use Shopware\Core\Framework\DataAbstractionLayer\TranslationEntity;
use TeuDocument\Core\Content\DocumentCategory\DocumentCategoryEntity;

class DocumentCategoryTranslationEntity extends TranslationEntity
{
    use EntityCustomFieldsTrait;
    protected ?string $documentCategoryId = null;
    protected ?string $name = null;
    protected ?array $breadcrumb = null;
    protected ?string $description = null;

    protected ?DocumentCategoryEntity $documentCategory = null;

    /**
     * @return string|null
     */
    public function getDocumentCategoryId(): ?string
    {
        return $this->documentCategoryId;
    }

    /**
     * @param string|null $documentCategoryId
     */
    public function setDocumentCategoryId(?string $documentCategoryId): void
    {
        $this->documentCategoryId = $documentCategoryId;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function getBreadcrumb(): ?array
    {
        return $this->breadcrumb;
    }

    public function setBreadcrumb(?array $breadcrumb): void
    {
        $this->breadcrumb = $breadcrumb;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return DocumentCategoryEntity|null
     */
    public function getDocumentCategory(): ?DocumentCategoryEntity
    {
        return $this->documentCategory;
    }

    /**
     * @param DocumentCategoryEntity|null $documentCategory
     */
    public function setDocumentCategory(?DocumentCategoryEntity $documentCategory): void
    {
        $this->documentCategory = $documentCategory;
    }

    

}
