<?php declare(strict_types=1);

namespace TeuDocument\Core\Content\DocumentCategory\Aggregate\DocumentCategoryTranslation;

use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

/**
 * @method void              add(DocumentCategoryTranslationEntity $entity)
 * @method void              set(string $key, DocumentCategoryTranslationEntity $entity)
 * @method DocumentCategoryTranslationEntity[]    getIterator()
 * @method DocumentCategoryTranslationEntity[]    getElements()
 * @method DocumentCategoryTranslationEntity|null get(string $key)
 * @method DocumentCategoryTranslationEntity|null first()
 * @method DocumentCategoryTranslationEntity|null last()
 */
class DocumentCategoryTranslationCollection extends EntityCollection
{
    protected function getExpectedClass(): string
    {
        return DocumentCategoryTranslationEntity::class;
    }
}
