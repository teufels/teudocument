<?php declare(strict_types=1);

namespace TeuDocument\Core\Content\DocumentCategory\Aggregate\DocumentCategoryTranslation;


use Shopware\Core\Framework\DataAbstractionLayer\Field\BreadcrumbField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\CustomFields;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\ApiAware;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\WriteProtected;
use Shopware\Core\Framework\DataAbstractionLayer\Field\LongTextField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\StringField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\UpdatedAtField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;
use Shopware\Core\Framework\DataAbstractionLayer\EntityTranslationDefinition;
use TeuDocument\Core\Content\DocumentCategory\DocumentCategoryDefinition;

class DocumentCategoryTranslationDefinition extends EntityTranslationDefinition
{
    final public const ENTITY_NAME = 'teu_product_document_category_translation';

    public function getEntityName(): string
    {
        return self::ENTITY_NAME;
    }
    public function getCollectionClass(): string
    {
        return DocumentCategoryTranslationCollection::class;
    }

    public function getParentDefinitionClass(): string
    {
        return DocumentCategoryDefinition::class;
    }

    public function getEntityClass(): string
    {
        return DocumentCategoryTranslationEntity::class;
    }

    protected function defineFields(): FieldCollection
    {
        return new FieldCollection([
            (new StringField('name', 'name'))->addFlags(new Required()),
            (new BreadcrumbField())->addFlags(new ApiAware(), new WriteProtected()),
            (new LongTextField('description', 'description')),
            new CustomFields()
        ]);
    }
}
