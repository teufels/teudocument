<?php declare(strict_types=1);

namespace TeuDocument\Core\Content\DocumentCategory\Tree;


use Shopware\Core\Framework\Log\Package;
use Shopware\Core\Framework\Struct\Struct;
use TeuDocument\Core\Content\DocumentCategory\DocumentCategoryEntity;

#[Package('content')]
class DocumentCategoryTree extends Struct
{
    /**
     * @var DocumentCategoryTreeItem[]
     */
    protected $tree;

    /**
     * @var DocumentCategoryEntity|null
     */
    protected $active;

    /**
     * @param DocumentCategoryTreeItem[] $tree
     */
    public function __construct(
        ?DocumentCategoryEntity $active,
        array $tree
    ) {
        $this->tree = $tree;
        $this->active = $active;
    }

    public function isSelected(DocumentCategoryEntity $category): bool
    {
        if ($this->active === null) {
            return false;
        }

        if ($category->getId() === $this->active->getId()) {
            return true;
        }

        if (!$this->active->getPath()) {
            return false;
        }

        $ids = explode('|', $this->active->getPath());

        return \in_array($category->getId(), $ids, true);
    }

    /**
     * @return DocumentCategoryTreeItem[]
     */
    public function getTree(): array
    {
        return $this->tree;
    }

    /**
     * @param DocumentCategoryTreeItem[] $tree
     */
    public function setTree(array $tree): void
    {
        $this->tree = $tree;
    }

    public function getActive(): ?DocumentCategoryEntity
    {
        return $this->active;
    }

    public function setActive(?DocumentCategoryEntity $active): void
    {
        $this->active = $active;
    }

    public function getChildren(string $categoryId): ?DocumentCategoryTreeItem
    {
        $match = $this->find($categoryId, $this->tree);

        if ($match) {
            return new DocumentCategoryTreeItem($match->getCategory(), $match->getChildren());
        }

        // active id is not part of $this->tree? active id is root or used as first level
        if ($this->active && $this->active->getId() === $categoryId) {
            return $this;
        }

        return null;
    }

    public function getApiAlias(): string
    {
        return 'document_category_tree';
    }

    /**
     * @param DocumentCategoryTreeItem[] $tree
     */
    private function find(string $categoryId, array $tree): ?DocumentCategoryTreeItem
    {
        if (isset($tree[$categoryId])) {
            return $tree[$categoryId];
        }

        foreach ($tree as $item) {
            $nested = $this->find($categoryId, $item->getChildren());

            if ($nested) {
                return $nested;
            }
        }

        return null;
    }
}
