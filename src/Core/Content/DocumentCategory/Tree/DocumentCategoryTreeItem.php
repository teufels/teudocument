<?php declare(strict_types=1);

namespace TeuDocument\Core\Content\DocumentCategory\Tree;

use Shopware\Core\Content\Category\CategoryException;
use Shopware\Core\Framework\Log\Package;
use Shopware\Core\Framework\Struct\Struct;
use TeuDocument\Core\Content\DocumentCategory\DocumentCategoryEntity;

#[Package('content')]
class DocumentCategoryTreeItem extends Struct
{
    /**
     * @internal public to allow AfterSort::sort()
     */
    public ?string $afterId;

    /**
     * @var DocumentCategoryEntity|null
     */
    protected $category;

    /**
     * @var DocumentCategoryTreeItem[]
     */
    protected $children;

    /**
     * @param DocumentCategoryTreeItem[] $children
     */
    public function __construct(
        ?DocumentCategoryEntity $category,
        array $children
    ) {
        $this->category = $category;
        $this->children = $children;
        $this->afterId = $category?->getAfterCategoryId();
    }

    public function getId(): string
    {
        return $this->getCategory()->getId();
    }

    public function setCategory(DocumentCategoryEntity $category): void
    {
        $this->category = $category;
        $this->afterId = $category->getAfterCategoryId();
    }

    public function getCategory(): DocumentCategoryEntity
    {
        if (!$this->category) {
            throw CategoryException::categoryNotFound('treeItem');
        }

        return $this->category;
    }

    /**
     * @return DocumentCategoryTreeItem[]
     */
    public function getChildren(): array
    {
        return $this->children;
    }

    public function addChildren(DocumentCategoryTreeItem ...$items): void
    {
        foreach ($items as $item) {
            $this->children[] = $item;
        }
    }

    /**
     * @param DocumentCategoryTreeItem[] $children
     */
    public function setChildren(array $children): void
    {
        $this->children = $children;
    }

    public function getApiAlias(): string
    {
        return 'document_category_tree_item';
    }
}
