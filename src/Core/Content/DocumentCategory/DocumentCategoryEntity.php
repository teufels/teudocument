<?php declare(strict_types=1);

namespace TeuDocument\Core\Content\DocumentCategory;

use Enqueue\Dbal\JSON;
use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityCustomFieldsTrait;
use Shopware\Core\Framework\DataAbstractionLayer\EntityIdTrait;
use TeuDocument\Core\Content\Document\DocumentCollection;
use TeuDocument\Core\Content\DocumentCategory\Aggregate\DocumentCategoryTranslation\DocumentCategoryTranslationCollection;

class DocumentCategoryEntity extends Entity
{
    use EntityIdTrait;
    use EntityCustomFieldsTrait;

    protected ?string $parentId = null;
    protected ?string $afterCategoryId = null;

    protected ?string $name = null;
    protected ?array $breadcrumb =null;
    protected ?string $description = null;

    protected ?string $path = null;
    protected ?int $level;
    protected ?int $childCount;

    protected ?bool $active = null;

    protected ?DocumentCategoryTranslationCollection $translations = null;

    protected ?DocumentCategoryEntity $parent = null;
    protected ?DocumentCategoryCollection $children = null;
    protected ?DocumentCollection $documents = null;

    /**
     * @return string|null
     */
    public function getParentId(): ?string
    {
        return $this->parentId;
    }

    /**
     * @param string|null $parentId
     */
    public function setParentId(?string $parentId): void
    {
        $this->parentId = $parentId;
    }

    /**
     * @return string|null
     */
    public function getAfterCategoryId(): ?string
    {
        return $this->afterCategoryId;
    }

    /**
     * @param string|null $afterCategoryId
     */
    public function setAfterCategoryId(?string $afterCategoryId): void
    {
        $this->afterCategoryId = $afterCategoryId;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return array<mixed>
     */
    public function getBreadcrumb(): array
    {
        return array_values($this->getPlainBreadcrumb());
    }

    /**
     * @return array<mixed>
     */
    public function getPlainBreadcrumb(): array
    {
        $breadcrumb = $this->getTranslation('breadcrumb');
        if ($breadcrumb === null) {
            return [];
        }
        if ($this->path === null) {
            return $breadcrumb;
        }

        $parts = \array_slice(explode('|', $this->path), 1, -1);

        $filtered = [];
        foreach ($parts as $id) {
            if (isset($breadcrumb[$id])) {
                $filtered[$id] = $breadcrumb[$id];
            }
        }

        $filtered[$this->getId()] = $breadcrumb[$this->getId()];

        return $filtered;
    }

    /**
     * @param array<mixed>|null $breadcrumb
     */
    public function setBreadcrumb(?array $breadcrumb): void
    {
        $this->breadcrumb = $breadcrumb;
    }

    /**
     * @return array<mixed>
     */
    public function jsonSerialize(): array
    {
        // Make sure that the sorted breadcrumb gets serialized
        $data = parent::jsonSerialize();
        $data['translated']['breadcrumb'] = $data['breadcrumb'] = $this->getBreadcrumb();

        return $data;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string|null
     */
    public function getPath(): ?string
    {
        return $this->path;
    }

    /**
     * @param string|null $path
     */
    public function setPath(?string $path): void
    {
        $this->path = $path;
    }

    /**
     * @return int|null
     */
    public function getLevel(): ?int
    {
        return $this->level;
    }

    /**
     * @param int|null $level
     */
    public function setLevel(?int $level): void
    {
        $this->level = $level;
    }

    /**
     * @return int|null
     */
    public function getChildCount(): ?int
    {
        return $this->childCount;
    }

    /**
     * @param int|null $childCount
     */
    public function setChildCount(?int $childCount): void
    {
        $this->childCount = $childCount;
    }

    /**
     * @return bool|null
     */
    public function getActive(): ?bool
    {
        return $this->active;
    }

    /**
     * @param bool|null $active
     */
    public function setActive(?bool $active): void
    {
        $this->active = $active;
    }

    /**
     * @return DocumentCategoryTranslationCollection|null
     */
    public function getTranslations(): ?DocumentCategoryTranslationCollection
    {
        return $this->translations;
    }

    /**
     * @param DocumentCategoryTranslationCollection|null $translations
     */
    public function setTranslations(?DocumentCategoryTranslationCollection $translations): void
    {
        $this->translations = $translations;
    }

    /**
     * @return DocumentCategoryEntity|null
     */
    public function getParent(): ?DocumentCategoryEntity
    {
        return $this->parent;
    }

    /**
     * @param DocumentCategoryEntity|null $parent
     */
    public function setParent(?DocumentCategoryEntity $parent): void
    {
        $this->parent = $parent;
    }

    /**
     * @return DocumentCategoryCollection|null
     */
    public function getChildren(): ?DocumentCategoryCollection
    {
        return $this->children;
    }

    /**
     * @param DocumentCategoryCollection|null $children
     */
    public function setChildren(?DocumentCategoryCollection $children): void
    {
        $this->children = $children;
    }

    /**
     * @return DocumentCollection|null
     */
    public function getDocuments(): ?DocumentCollection
    {
        return $this->documents;
    }

    /**
     * @param DocumentCollection|null $documents
     */
    public function setDocuments(?DocumentCollection $documents): void
    {
        $this->documents = $documents;
    }



}
