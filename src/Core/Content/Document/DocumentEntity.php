<?php declare(strict_types=1);

namespace TeuDocument\Core\Content\Document;

use Enqueue\Dbal\JSON;
use Shopware\Core\Content\Media\MediaEntity;
use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityCustomFieldsTrait;
use Shopware\Core\Framework\DataAbstractionLayer\EntityIdTrait;
use Shopware\Core\System\Tag\TagCollection;
use TeuDocument\Core\Content\DocumentCategory\DocumentCategoryCollection;

class DocumentEntity extends Entity
{
    use EntityIdTrait;
    use EntityCustomFieldsTrait;

    protected ?string $name = null;
    protected ?string $description = null;
    protected ?string $externalLink = null;

    protected ?bool $isExternal = null;
    protected ?bool $externalNewTab = null;

    protected ?string $mediaId = null;
    protected ?MediaEntity $media = null;

    protected ?string $previewId = null;
    protected ?MediaEntity $previewMedia = null;

    protected ?DocumentCategoryCollection $categories = null;
    protected ?TagCollection $tags = null;

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string|null
     */
    public function getExternalLink(): ?string
    {
        return $this->externalLink;
    }

    /**
     * @param string|null $externalLink
     */
    public function setExternalLink(?string $externalLink): void
    {
        $this->externalLink = $externalLink;
    }

    /**
     * @return bool|null
     */
    public function getIsExternal(): ?bool
    {
        return $this->isExternal;
    }

    /**
     * @param bool|null $isExternal
     */
    public function setIsExternal(?bool $isExternal): void
    {
        $this->isExternal = $isExternal;
    }

    /**
     * @return bool|null
     */
    public function getExternalNewTab(): ?bool
    {
        return $this->externalNewTab;
    }

    /**
     * @param bool|null $externalNewTab
     */
    public function setExternalNewTab(?bool $externalNewTab): void
    {
        $this->externalNewTab = $externalNewTab;
    }

    /**
     * @return string|null
     */
    public function getMediaId(): ?string
    {
        return $this->mediaId;
    }

    /**
     * @param string|null $mediaId
     */
    public function setMediaId(?string $mediaId): void
    {
        $this->mediaId = $mediaId;
    }

    /**
     * @return MediaEntity|null
     */
    public function getMedia(): ?MediaEntity
    {
        return $this->media;
    }

    /**
     * @param MediaEntity|null $media
     */
    public function setMedia(?MediaEntity $media): void
    {
        $this->media = $media;
    }

    /**
     * @return string|null
     */
    public function getPreviewId(): ?string
    {
        return $this->previewId;
    }

    /**
     * @param string|null $previewId
     */
    public function setPreviewId(?string $previewId): void
    {
        $this->previewId = $previewId;
    }

    /**
     * @return MediaEntity|null
     */
    public function getPreviewMedia(): ?MediaEntity
    {
        return $this->previewMedia;
    }

    /**
     * @param MediaEntity|null $previewMedia
     */
    public function setPreviewMedia(?MediaEntity $previewMedia): void
    {
        $this->previewMedia = $previewMedia;
    }

    /**
     * @return DocumentCategoryCollection|null
     */
    public function getCategories(): ?DocumentCategoryCollection
    {
        return $this->categories;
    }

    /**
     * @param DocumentCategoryCollection|null $categories
     */
    public function setCategories(?DocumentCategoryCollection $categories): void
    {
        $this->categories = $categories;
    }

    /**
     * @return TagCollection|null
     */
    public function getTags(): ?TagCollection
    {
        return $this->tags;
    }

    /**
     * @param TagCollection|null $tags
     */
    public function setTags(?TagCollection $tags): void
    {
        $this->tags = $tags;
    }


}
