<?php declare(strict_types=1);

namespace TeuDocument\Core\Content\Document;

use Shopware\Core\Content\Media\MediaDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\BoolField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\CreatedAtField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\ApiAware;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\PrimaryKey;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IdField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ManyToManyAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\OneToManyAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\OneToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\StringField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\TranslatedField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\TranslationsAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\UpdatedAtField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ManyToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\FkField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\JsonField;
use Shopware\Core\System\Tag\TagDefinition;
use TeuDocument\Core\Content\Document\Aggregate\DocumentCategoryMappingDefinition;
use TeuDocument\Core\Content\Document\Aggregate\DocumentTag\DocumentTagDefinition;
use TeuDocument\Core\Content\Document\Aggregate\DocumentTranslation\DocumentTranslationDefinition;
use TeuDocument\Core\Content\DocumentCategory\DocumentCategoryDefinition;
use TeuDocument\Core\Content\DocumentCategory\DocumentCategoryEntity;

class DocumentDefinition extends EntityDefinition
{
    final public const ENTITY_NAME = 'teu_product_document';

    public function getEntityName(): string
    {
        return self::ENTITY_NAME;
    }

    public function getCollectionClass(): string
    {
        return DocumentCollection::class;
    }

    public function getEntityClass(): string
    {
        return DocumentEntity::class;
    }

    protected function defineFields(): FieldCollection
    {
        return new FieldCollection([
            (new IdField('id', 'id'))->addFlags(new PrimaryKey(), new Required()),


            (new TranslatedField('name'))->addFlags(new Required()),
            (new TranslatedField('description')),
            (new TranslatedField('mediaId')),
            (new TranslatedField('previewId')),
            (new TranslationsAssociationField(
                DocumentTranslationDefinition::class,
                'teu_product_document_id'
            ))->addFlags(new Required()),
            (new TranslatedField('externalLink')),
            (new BoolField('is_external','isExternal')),
            (new BoolField('external_new_tab','externalNewTab')),
            (new CreatedAtField()),
            (new UpdatedAtField()),

            (new TranslatedField('customFields'))->addFlags(new ApiAware()),

            //new OneToOneAssociationField('documentId','document_id','documentId',DocumentTranslationDefinition::class,fale),
            new ManyToManyAssociationField('tags', TagDefinition::class, DocumentTagDefinition::class, 'teu_product_document_id', 'tag_id'),
            new ManyToManyAssociationField('categories', DocumentCategoryDefinition::class, DocumentCategoryMappingDefinition::class, 'teu_document_id', 'teu_document_category_id'),
        ]);
    }
}
