<?php declare(strict_types=1);

namespace TeuDocument\Core\Content\Document\Aggregate\DocumentTranslation;

use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

/**
 * @method void              add(DocumentTranslationEntity $entity)
 * @method void              set(string $key, DocumentTranslationEntity $entity)
 * @method DocumentTranslationEntity[]    getIterator()
 * @method DocumentTranslationEntity[]    getElements()
 * @method DocumentTranslationEntity|null get(string $key)
 * @method DocumentTranslationEntity|null first()
 * @method DocumentTranslationEntity|null last()
 */
class DocumentTranslationCollection extends EntityCollection
{
    protected function getExpectedClass(): string
    {
        return DocumentTranslationEntity::class;
    }
}
