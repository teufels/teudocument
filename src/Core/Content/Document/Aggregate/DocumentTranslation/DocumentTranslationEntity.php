<?php declare(strict_types=1);

namespace TeuDocument\Core\Content\Document\Aggregate\DocumentTranslation;

use Enqueue\Dbal\JSON;
use Shopware\Core\Content\Media\MediaEntity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityCustomFieldsTrait;
use Shopware\Core\Framework\DataAbstractionLayer\TranslationEntity;
use TeuDocument\Core\Content\Document\DocumentEntity;

class DocumentTranslationEntity extends TranslationEntity
{
    use EntityCustomFieldsTrait;


    protected ?string $name = null;
    protected ?string $description = null;
    protected ?string $externalLink = null;

    protected ?string $mediaId = null;
    protected ?MediaEntity $media = null;

    protected ?string $previewId = null;
    protected ?MediaEntity $preview = null;

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string|null
     */
    public function getExternalLink(): ?string
    {
        return $this->externalLink;
    }

    /**
     * @param string|null $externalLink
     */
    public function setExternalLink(?string $externalLink): void
    {
        $this->externalLink = $externalLink;
    }

    /**
     * @return string|null
     */
    public function getMediaId(): ?string
    {
        return $this->mediaId;
    }

    /**
     * @param string|null $mediaId
     */
    public function setMediaId(?string $mediaId): void
    {
        $this->mediaId = $mediaId;
    }

    /**
     * @return MediaEntity|null
     */
    public function getMedia(): ?MediaEntity
    {
        return $this->media;
    }

    /**
     * @param MediaEntity|null $media
     */
    public function setMedia(?MediaEntity $media): void
    {
        $this->media = $media;
    }

    /**
     * @return string|null
     */
    public function getPreviewId(): ?string
    {
        return $this->previewId;
    }

    /**
     * @param string|null $previewId
     */
    public function setPreviewId(?string $previewId): void
    {
        $this->previewId = $previewId;
    }

    /**
     * @return MediaEntity|null
     */
    public function getPreview(): ?MediaEntity
    {
        return $this->preview;
    }

    /**
     * @param MediaEntity|null $preview
     */
    public function setPreview(?MediaEntity $preview): void
    {
        $this->preview = $preview;
    }



}
