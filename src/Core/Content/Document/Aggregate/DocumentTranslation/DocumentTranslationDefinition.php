<?php declare(strict_types=1);

namespace TeuDocument\Core\Content\Document\Aggregate\DocumentTranslation;

use Shopware\Core\Content\Media\MediaDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\BoolField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\CreatedAtField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\CustomFields;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\AllowHtml;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\ApiAware;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\PrimaryKey;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IdField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\LongTextField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\StringField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\UpdatedAtField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ManyToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\FkField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\JsonField;
use Shopware\Core\Framework\DataAbstractionLayer\EntityTranslationDefinition;
use TeuDocument\Core\Content\Document\DocumentDefinition;

class DocumentTranslationDefinition extends EntityTranslationDefinition
{
    final public const ENTITY_NAME = 'teu_product_document_translation';

    public function getEntityName(): string
    {
        return self::ENTITY_NAME;
    }

    public function getParentDefinitionClass(): string
    {
        return DocumentDefinition::class;
    }

    public function getEntityClass(): string
    {
        return DocumentTranslationEntity::class;
    }

    protected function defineFields(): FieldCollection
    {
        return new FieldCollection([

            new FkField('media_id', 'mediaId', MediaDefinition::class),
            new FkField('preview_id', 'previewId', MediaDefinition::class),

            (new StringField('name', 'name'))->addFlags(new Required()),
            (new LongTextField('description', 'description')),
            (new StringField('external_link', 'externalLink')),

            (new CustomFields())->addFlags(new ApiAware()),

            new ManyToOneAssociationField('media', 'media_id', MediaDefinition::class, 'id', false),
            new ManyToOneAssociationField('previewMedia', 'preview_id', MediaDefinition::class, 'id', false),

        ]);
    }
}
