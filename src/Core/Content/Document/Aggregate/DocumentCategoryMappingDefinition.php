<?php declare(strict_types=1);

namespace  TeuDocument\Core\Content\Document\Aggregate;

use Shopware\Core\Framework\DataAbstractionLayer\Field\FkField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\PrimaryKey;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ManyToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;
use Shopware\Core\Framework\DataAbstractionLayer\MappingEntityDefinition;
use TeuDocument\Core\Content\Document\DocumentDefinition;
use TeuDocument\Core\Content\DocumentCategory\DocumentCategoryDefinition;

class DocumentCategoryMappingDefinition extends MappingEntityDefinition
{
    public const ENTITY_NAME = 'teu_product_document_document_category';

    public function getEntityName(): string
    {
        return self::ENTITY_NAME;
    }

    protected function defineFields(): FieldCollection
    {
        return new FieldCollection([
            (new FkField('teu_document_id', 'documentId', DocumentDefinition::class))->addFlags(new PrimaryKey(), new Required()),
            (new FkField('teu_document_category_id', 'documentCategoryId', DocumentCategoryDefinition::class))->addFlags(new PrimaryKey(), new Required()),

            new ManyToOneAssociationField('document', 'teu_document_id', DocumentDefinition::class, 'id', false),
            new ManyToOneAssociationField('documentCategory', 'teu_document_category_id', DocumentCategoryDefinition::class, 'id', false),
        ]);
    }
}
