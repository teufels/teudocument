<?php declare(strict_types=1);

namespace TeuDocument\Core\Cms;

use Couchbase\Document;
use Shopware\Core\Content\Cms\Aggregate\CmsSlot\CmsSlotEntity;
use Shopware\Core\Content\Cms\DataResolver\CriteriaCollection;
use Shopware\Core\Content\Cms\DataResolver\Element\AbstractCmsElementResolver;
use Shopware\Core\Content\Cms\DataResolver\Element\ElementDataCollection;
use Shopware\Core\Content\Cms\DataResolver\ResolverContext\EntityResolverContext;
use Shopware\Core\Content\Cms\DataResolver\ResolverContext\ResolverContext;
use Shopware\Core\Content\Cms\SalesChannel\Struct\ProductBoxStruct;
use Shopware\Core\Content\Product\ProductDefinition;
use Shopware\Core\Content\Product\SalesChannel\SalesChannelProductEntity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\ContainsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsAnyFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\MultiFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Sorting\FieldSorting;
use Shopware\Core\Framework\Struct\ArrayEntity;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use TeuDocument\Core\Content\Document\DocumentDefinition;
use TeuDocument\Service\MediaHandleService;

class DocumentCmsElementResolver extends AbstractCmsElementResolver
{

    public function __construct(
        private readonly SystemConfigService $systemConfigService,
        private readonly MediaHandleService $mediaHandleService
    )
    {
    }

    public function getType(): string
    {
        return 'teu-document'; // technischer Name des Elements
    }

    public function collect(CmsSlotEntity $slot, ResolverContext $resolverContext): ?CriteriaCollection
    {
        // Konfiguration des Elements auf der CMS Seite (Daten aus Administration)
        $config = $slot->getFieldConfig();

        // z.B. ein konfiguriertes Produkt, das nun aufgelöst werden soll
        $typeOfListing =$config->get('typeOfListing')->getValue();

        $criteria = new Criteria();
        $criteriaCollection = new CriteriaCollection();
        $criteriaCollection->add('teu_product_document', DocumentDefinition::class, $criteria);
        $criteria->addAssociation('tags');
        $criteria->addAssociation('categories');

        if ($typeOfListing == 'tag') {
            if($config->get('listingTags')->getValue() != null && count($config->get('listingTags')->getValue()) > 0) {
                $criteria->addFilter(new EqualsAnyFilter('tags.id', $config->get('listingTags')->getValue()));
            }
        } elseif ($typeOfListing == 'category') {
            $criteria->addFilter(new EqualsFilter('categories.active', true));
            if($config->get('listingCategories')->getValue() != null && count($config->get('listingCategories')->getValue()) > 0) {
                $filter = [];
                foreach ($config->get('listingCategories')->getValue() as $category) {
                    $filter[] = new EqualsFilter('categories.id', $category);
                    $filter[] = new ContainsFilter('categories.path', '|' . $category . '|');
                }
                $criteria->addFilter( new MultiFilter(
                    MultiFilter::CONNECTION_OR,
                    $filter
                 ));
            }
        } elseif ($typeOfListing == 'manual') {
            $criteria->addFilter(new EqualsAnyFilter('id', $config->get('listingSelection')->getValue()));
        }

        $criteria->addSorting(new FieldSorting('name', FieldSorting::ASCENDING));


        return $criteriaCollection;
    }

    public function enrich(CmsSlotEntity $slot, ResolverContext $resolverContext, ElementDataCollection $result): void
    {
        $config = $slot->getFieldConfig();
        $typeOfListing =$config->get('typeOfListing')->getValue();
        $showPreview = $config->get('listingShowPreview')->getValue();

        $data = new ArrayEntity();
        $slot->setData($data);

        if ($typeOfListing != 'customField') {
            $docs = $result->get('teu_product_document');
            foreach ($docs->getElements() as $doc) {
                if ($showPreview) {
                    //print_r($productDoc);die;
                    if ($doc->getTranslated()['previewId']) {
                        //continue;
                    } elseif (!isset($doc->getTranslated()['mediaId'])) {
                        //continue;
                    } elseif ($doc->isExternal) {
                        //continue;
                    } else {
                        $tranlated = $doc->getTranslated();
                        $tranlated['previewId'] =  $this->mediaHandleService->checkPreviewForMedia([$doc->getTranslated()['mediaId']]);
                        $doc->setTranslated($tranlated);
                    }
                }
            }
            $data->set('documents', $docs);
        }
    }


}
