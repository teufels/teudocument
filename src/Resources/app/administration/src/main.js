import './component/entity/teu-document-category-tree-field'

import './module/teu-document';
import './module/teu-document-category';

import './module/sw-cms/elements/teu-document';
import './module/sw-cms/blocks/teufels/teu-document';

Shopware.Component.override('sw-admin-menu', {
    inject: [
        'customFieldDataProviderService'
    ],

    mounted: function() {
        this.customFieldDataProviderService.addEntityName('teu_product_document');
        this.customFieldDataProviderService.addEntityName('teu_product_document_category');
    }
});
