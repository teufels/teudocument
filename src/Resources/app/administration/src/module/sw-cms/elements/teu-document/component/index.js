const { Component, Mixin } = Shopware;
import template from './sw-cms-el-document.html.twig';
import './sw-cms-el-document.scss';

Component.register('sw-cms-el-document', {
    template,

    mixins: [
        Mixin.getByName('cms-element')
    ],

    computed: {
        assetFilter() {
            return Shopware.Filter.getByName('asset');
        }
    },

    created() {
        this.createdComponent();
    },

    methods: {
        createdComponent() {
            this.initElementConfig('teu-document');
        }
    }
});