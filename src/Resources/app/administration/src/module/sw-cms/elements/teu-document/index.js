import './component';
import './config';
import './preview';

const Criteria = Shopware.Data.Criteria;
const criteria = new Criteria();

Shopware.Service('cmsService').registerCmsElement({
    name: 'teu-document',
    label: 'sw-cms.elements.customTeuDocument.label',
    component: 'sw-cms-el-document',
    configComponent: 'sw-cms-el-config-document',
    previewComponent: 'sw-cms-el-preview-document',
    defaultConfig: {

        listingTemplate: { source: 'static', value: 'gallery' },

        typeOfListing: { source: 'static', value: 'tag' },

        listingTags: { source: 'static', value: null },

        listingShowPreview: { source: 'static', value: false },

        listingShowDescription: { source: 'static', value: false },

        listingCategories: {
            source: 'static',
            value: null,
            entity: {
                name: 'teu_product_document_category',
                criteria: criteria
            }
        },

        listingSelection: {
            source: 'static',
            value: null,
            entity: {
                name: 'teu_product_document',
                criteria: criteria
            }
        },
    }
});
