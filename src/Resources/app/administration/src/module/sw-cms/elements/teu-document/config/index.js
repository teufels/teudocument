const { Component, Mixin } = Shopware;
const { EntityCollection } = Shopware.Data;
const Criteria = Shopware.Data.Criteria;
import template from './sw-cms-el-config-document.html.twig';

Component.register('sw-cms-el-config-document', {
    template,

    inject: [
        'repositoryFactory'
    ],

    data() {
        return {
            documentRepository: null,
            documentCategoryRepository: null,
            tagRepository: null,
            documents: [],
            tagsCollection: null,
            selectedCategories: null,
            selectedTags: null,
        };
    },

    mixins: [
        Mixin.getByName('cms-element')
    ],

    computed: {
        documentRepository() {
            return this.repositoryFactory.create('teu_product_document');
        },

        documentSelectContext() {
            const context = Object.assign({}, Shopware.Context.api);
            context.inheritance = true;

            return context;
        },

        documentCriteria() {
            const criteria = new Criteria();

            return criteria;
        },

        documentCategoryRepository() {
            return this.repositoryFactory.create('teu_product_document_category');
        },

        documentCategoriesConfigValue() {
            return this.element.config.listingCategories.value;
        },

        documentTagsConfigValue() {
            return this.element.config.listingTags.value;
        },

    },

    watch: {
        selectedCategories: {
            handler(value) {
                this.element.config.listingCategories.value = value.getIds();
                this.$set(this.element.data, 'listingCategories', value);
                this.$emit('element-update', this.element);
            },
        },
        selectedTags: {
            handler(value) {
                this.element.config.listingTags.value = value.getIds();
                this.$set(this.element.data, 'listingTags', value);
                this.$emit('element-update', this.element);
            },
        },
    },

    created() {
        this.tagsCollection = new EntityCollection('/tags', 'tag', Shopware.Context.api);
        this.documentRepository = this.repositoryFactory.create('teu_product_document');
        this.documentCategoryRepository = this.repositoryFactory.create('teu_product_document_category');
        this.tagRepository = this.repositoryFactory.create('tag');
        this.getDocuments();
        this.createdComponent();
    },

    methods: {
        async createdComponent() {
            this.initElementConfig('teu-document');


            await this.getSelectedTags();
            await this.getSelectedCategories();
            await this.getSelectedTags();
        },

        getDocuments() {
            this.documentRepository.search(new Criteria(), Shopware.Context.api).then((result) => {
                this.documents = result;
            });
        },

        onTagsChange() {
            this.element.config.listingTags.value = this.tagsCollection.getIds();
        },

        getSelectedCategories() {
            if (!Shopware.Utils.types.isEmpty(this.documentCategoriesConfigValue)) {
                const criteria = new Criteria();
                criteria.setIds(this.documentCategoriesConfigValue);

                this.documentCategoryRepository
                    .search(criteria, Shopware.Context.api)
                    .then((result) => {
                        this.selectedCategories = result;
                    });
            } else {
                this.selectedCategories = new EntityCollection(
                    this.documentCategoryRepository.route,
                    this.documentCategoryRepository.schema.entity,
                    Shopware.Context.api,
                    new Criteria(),
                );
            }
        },

        getSelectedTags() {
            if (!Shopware.Utils.types.isEmpty(this.documentTagsConfigValue)) {
                const criteria = new Criteria();
                criteria.setIds(this.documentTagsConfigValue);

                this.tagRepository
                    .search(criteria, Shopware.Context.api)
                    .then((result) => {
                        this.tagsCollection = result;
                    });
            } else {
                this.tagsCollection = new EntityCollection(
                    this.tagRepository.route,
                    this.tagRepository.schema.entity,
                    Shopware.Context.api,
                    new Criteria(),
                );
            }
        }
    }
});
