const { Component } = Shopware;
import template from './sw-cms-el-preview-document.html.twig';
import './sw-cms-el-preview-document.scss';

Component.register('sw-cms-el-preview-document', {
    template
});
