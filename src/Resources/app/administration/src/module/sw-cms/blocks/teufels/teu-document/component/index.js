/* global Shopware */

import template from './sw-cms-block-teu-document.html.twig';

Shopware.Component.register('sw-cms-block-teu-document', {
    template
});
