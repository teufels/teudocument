/* global Shopware */

import template from './sw-cms-block-preview-teu-document.html.twig';
import './sw-cms-block-preview-teu-document.scss';

Shopware.Component.register('sw-cms-block-preview-teu-document', {
    template
});
