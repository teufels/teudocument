/* global Shopware */

import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
    name: 'teu-document',
    label: 'sw-cms.elements.customTeuDocument.label',
    category: 'teu-cmspack',
    component: 'sw-cms-block-teu-document',
    previewComponent: 'sw-cms-block-preview-teu-document',
    defaultConfig: {
        marginBottom: '20px',
        marginTop:    '20px',
        marginLeft:   '20px',
        marginRight:  '20px',
        sizingMode:   'boxed'
    },
    slots: {
        content: {
            type: 'teu-document'
        }
    }
});
