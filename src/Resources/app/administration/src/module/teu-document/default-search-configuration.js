const defaultSearchConfiguration = {
    _searchable: true,
    name: {
        _searchable: true,
        _score: 500,
    }
};

export default defaultSearchConfiguration;
