const { Component } = Shopware;
const { date } = Shopware.Utils.format;

Component.extend('teu-document-create', 'teu-document-detail', {
    methods: {
        getDocument() {
            this.document = this.repository.create(Shopware.Context.api);
        },

        onSave() {
            this.isLoading = true;
            this.isSaveSuccessful = false;

            this.repository
                .save(this.document, Shopware.Context.api, this.criteria)
                .then(() => {
                    this.isLoading = false;
                    this.isSaveSuccessful = true;
                    this.createNotificationSuccess({
                        title: this.$t('teu-document.detail.success-title'),
                        message: this.$t('teu-document.detail.success-message')
                    });
                    this.$router.push({ name: 'teu.document.detail', params: { id: this.document.id } });
                }).catch((exception) => {
                this.isLoading = false;
                this.createNotificationError({
                    title: this.$t('teu-document.detail.error-message'),
                    message: this.$tc('global.notification.notificationSaveErrorMessage', 0, {
                        entityName: this.$t('global.entities.teu_product_document'),
                    })
                });
            });
        }
    },
});
