
import template from './teu-document-detail.html.twig';

const { Component, Mixin, StateDeprecated } = Shopware;
const { Criteria } = Shopware.Data;
const { mapPropertyErrors } = Shopware.Component.getComponentHelper();

Component.register('teu-document-detail', {
    template,

    inject: [
        'repositoryFactory',
    ],

    mixins: [
        Mixin.getByName('placeholder'),
        Mixin.getByName('notification')
    ],

    metaInfo() {
        return {
            title: this.$createTitle()
        };
    },

    data() {
        return {
            document: null,
            isLoading: false,
            repository: null,
            customFieldSets: null,
            imageUploadTag: 'teu-media-image-upload-tag',
        };
    },

    computed: {
        ...mapPropertyErrors('document', ['name']),
        mediaItem() {
            return this.document !== null ? this.document.mediaId : null;
        },

        repository() {
            return this.repositoryFactory.create('teu_product_document');
        },

        mediaRepository() {
            return this.repositoryFactory.create('media');
        },

        customFieldSetRepository() {
            return this.repositoryFactory.create('custom_field_set');
        },

        customFieldSetCriteria() {
            const criteria = new Criteria();
            criteria.addFilter(Criteria.equals('relations.entityName', 'teu_product_document'));
            criteria.getAssociation('customFields').addSorting(Criteria.sort('config.customFieldPosition', 'ASC', true));

            return criteria;
        },

        criteria() {
            const criteria = new Criteria();
            criteria.addSorting(Criteria.sort('createdAt', 'DESC'));
            criteria.addAssociation('tags');
            criteria.addAssociation('categories');
            return criteria;
        },
    },

    watch: {
        '$route.params.id'() {
            this.createdComponent();
        }
    },

    created() {
        this.createdComponent();
    },

    methods: {

        createdComponent() {
            this.repository = this.repositoryFactory.create('teu_product_document');
            this.getDocument();
            this.getCustomFieldsets();

            if (this.document && this.document.isNew() &&
                Shopware.Context.api.languageId !== Shopware.Context.api.systemLanguageId) {
                Shopware.State.commit('context/setApiLanguageId', Shopware.Context.api.systemLanguageId);
            }

        },

        onChangeLanguage() {
            this.getDocument();
        },

        getDocument() {
            this.repository
                .get(this.$route.params.id, Shopware.Context.api,this.criteria)
                .then((entity) => {
                    this.isLoading = false;
                    this.document = entity;
                });
        },

        getCustomFieldsets() {
            this.customFieldSetRepository.search(this.customFieldSetCriteria, Shopware.Context.api)
                .then((customFieldSets) => {
                    this.customFieldSets = customFieldSets;
                });
        },

        onSave() {
            // set params
            this.isLoading = true;
            this.isSaveSuccessful = false;

            // save the affiliate
            this.repository
                .save(this.document, Shopware.Context.api, this.criteria)
                .then(() => {
                    this.getDocument();
                    this.isLoading = false;
                    this.isSaveSuccessful = true;
                    this.createNotificationSuccess({
                        title: this.$t('teu-document.detail.success-title'),
                        message: this.$t('teu-document.detail.success-message')
                    });

                }).catch((exception) => {
                this.isLoading = false;
                this.createNotificationError({
                    title: this.$t('teu-document.detail.error-message'),
                    message: this.$tc('global.notification.notificationSaveErrorMessage', 0, {
                        entityName: this.$t('global.entities.teu_product_document'),
                    })
                });
            });
        },

        onSetMediaItem({ targetId }) {
            this.mediaRepository.get(targetId, Shopware.Context.api).then((updatedMedia) => {
                this.document.mediaId = targetId;
                this.document.media = updatedMedia;
            });
        },

        onMediaDropped(dropItem) {
            this.onSetMediaItem({ targetId: dropItem.id });
        },

        onRemoveMediaItem() {
            this.document.mediaId = null;
            this.document.media = null;
        },

        openMediaSidebar() {
            this.$refs.mediaSidebarItem.openContent();
        },

        setMediaItemFromSidebar(sideBarMedia) {
            this.mediaRepository.get(sideBarMedia.id, Shopware.Context.api).then((media) => {
                this.document.mediaId = media.id;
                this.document.media = media;
            });
        },
    }
});

