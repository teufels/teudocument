import template from './teu-document-list.html.twig';
import './teu-document-list.scss';

const {Component, Mixin} = Shopware;
const {Criteria} = Shopware.Data;

Component.register('teu-document-list', {
    template,

    inject: [
        'pluginService',
        'repositoryFactory',
        'filterFactory'
    ],

    mixins: [
        Mixin.getByName('listing'),
        Mixin.getByName('notification')
    ],

    data() {
        return {
            documents: null,
            tagsFilter: null,
            categoryFilter: null,
            isLoading: true,
            sortBy: 'name',
            sortDirection: 'DESC',
            defaultFilters: [
                'tags-filter',
                'categories-filter',
                'external-filter'
            ],
            storeKey: 'grid.filter.teu_document',
            activeFilterNumber: 0,
            filterCriteria: [],
            total: 0,
        };
    },

    metaInfo() {
        return {
            title: this.$createTitle()
        };
    },

    computed: {
        columns() {
            return this.getColumns();
        },

        repository() {
            return this.getRepository();
        },
        documentCriteria() {
            const documentCriteria = new Criteria(this.page, this.limit);

            documentCriteria.setTerm(this.term);
            documentCriteria.addSorting(Criteria.sort(this.sortBy, this.sortDirection, this.naturalSorting));
            documentCriteria.addAssociation('tags');
            documentCriteria.addAssociation('categories');

            this.filterCriteria.forEach(filter => {
                documentCriteria.addFilter(filter);
            });

            return documentCriteria;
        },
        listFilterOptions() {
            return {
                'tags-filter': {
                    property: 'tags',
                    label: this.$tc('teu-document.filters.tagsFilter.label'),
                    placeholder: this.$tc('teu-document.filters.tagsFilter.placeholder'),
                },
                'categories-filter': {
                    property: 'categories',
                    label: this.$tc('teu-document.filters.categoriesFilter.label'),
                    placeholder: this.$tc('teu-document.filters.categoriesFilter.placeholder'),
                    displayPath: true,
                },
                'external-filter': {
                    property: 'isExternal',
                    label: this.$tc('teu-document.filters.externalFilter.label'),
                    placeholder: this.$tc('teu-document.filters.externalFilter.placeholder'),
                },
            };
        },
        listFilters() {
            return this.filterFactory.create('teu_product_document', this.listFilterOptions);
        },
    },

    watch: {
        documentCriteria: {
            handler() {
                this.getList();
            },
        },
    },

    methods: {
        generatePreview() {

            const client = Shopware.Application.getContainer('init').httpClient;
            const headers = {
                headers: {
                    Authorization: `Bearer ${Shopware.Service('loginService').getToken()}`,
                },
            };

            client.get('/teu-document/generate-preview', headers).then(response => {

                //const res = Shopware.Classes.ApiService.handleResponse(response);
                //console.log(res);
                //success
                this.createNotificationSuccess({
                    message: this.$tc(
                        'teu-document.generatePreviewHandler.success'
                    )
                });
            }).catch(() => {

                this.createNotificationError({
                    message: this.$tc('teu-document.generatePreviewHandler.error')
                });


            });


        },
        getColumns() {
            return [{
                property: 'name',
                dataIndex: 'name',
                label: this.$t('teu-document.list.column-name'),
                allowResize: true,
                routerLink: 'teu.document.detail',
                primary: true
            },
            {
                property: 'mediaId',
                dataIndex: 'mediaId',
                sortable: false,
                label: this.$t('teu-document.list.image')
            },
                {
                    property: 'tags',
                    dataIndex: 'tags.name',
                    sortable: true,
                    label: this.$t('teu-document.list.tags')
                },
                {
                    property: 'categories',
                    dataIndex: 'categories.name',
                    sortable: true,
                    label: this.$t('teu-document.list.categories')
                },
                {
                    property: 'isExternal',
                    dataIndex: 'isExternal',
                    sortable: true,
                    label: this.$t('teu-document.list.external'),
                    align: 'center',
                },
            ];
        },

        async getList() {
            this.isLoading = true;
            this.repository = this.repositoryFactory.create('teu_product_document');

            let criteria = await Shopware.Service('filterService')
                .mergeWithStoredFilters(this.storeKey, this.documentCriteria);

            criteria = await this.addQueryScores(this.term, criteria);
            /*const criteria = new Criteria();
            criteria
                .addSorting(Criteria.sort(this.sortBy, this.sortDirection))
                .addAssociation('tags')
                .addAssociation('categories');
            criteria.setTerm(this.term);
            if (this.tagsFilter) {
                criteria.addFilter(Criteria.equals('tags.id', this.tagsFilter));
            }
            if (this.categoryFilter) {
                criteria.addFilter(Criteria.equals('categories.id', this.categoryFilter));
            }*/

            this.activeFilterNumber = criteria.filters.length;

            /*this.filterCriteria.forEach(filter => {
                criteria.addFilter(filter);
            });*/

            this.repository
                .search(criteria, Shopware.Context.api)
                .then((result) => {
                    this.documents = result;
                    this.total = this.documents.total;
                    this.isLoading = false;
                });
        },

        onRefresh() {
            this.getList();
        },

        getRepository() {
            return this.repositoryFactory.create('teu_product_document');
        },

        onSearch(value = null) {
            if (!value.length || value.length <= 0) {
                this.term = null;
            } else {
                this.term = value;
            }

            this.resetList();
        },

        onChangeTagsFilter(value) {
            this.TagsFilter = value;
            this.getList();
        },

        onChangeCategoryFilter(value) {
            this.categoryFilter = value;
            this.getList();
        },

        updateTotal({ total }) {
            this.total = total;
        },

        resetList() {
            this.page = 1;
            this.pages = [];
            this.updateRoute({
                page: this.page,
                limit: this.limit,
                term: this.term,
                sortBy: this.sortBy,
                sortDirection: this.sortDirection
            });
            this.getList();
        },

        changeLanguage() {
            this.resetList();
        },


        updateCriteria(criteria) {
            this.page = 1;

            this.filterCriteria = criteria;
        },
    },

    created() {
    }
});
