const { Application } = Shopware;

import './page/teu-document-list';
import './page/teu-document-detail';
import './page/teu-document-create';

import deDE from './snippet/de-DE';
import enGB from './snippet/en-GB';

import defaultSearchConfiguration from './default-search-configuration';

/*import searchBarTemplate from './sw-search-bar-item.html.twig';
Shopware.Component.override('sw-search-bar-item', {
    template: searchBarTemplate
});*/

Shopware.Module.register('teu-document', {
    type: 'plugin',
    name: 'Document',
    title: 'teu-document.main.menuLabel',
    description: 'teu-document.main.menuDescription',
    color: '#000',
    icon: 'regular-books',
    entity: 'teu_product_document',

    snippets: {
        'de-DE': deDE,
        'en-GB': enGB
    },

   routes: {
        list: {
            component: 'teu-document-list',
            path: 'list'
        },
        detail: {
            component: 'teu-document-detail',
            path: 'detail/:id',
            meta: {
                parentPath: 'teu.document.list'
            }
        },
        create: {
            component: 'teu-document-create',
            path: 'create',
            meta: {
                parentPath: 'teu.document.list'
            }
        }
    },

    navigation: [{
        label: 'teu-document.main.menuLabel',
        color: '#000',
        path: 'teu.document.list',
        icon: 'regular-books',
        parent: 'sw-catalogue',
        position: 100
    }],

    defaultSearchConfiguration
});
const swVersion = Shopware.Context.app.config.version.split('.');
let searchTypeOptions = {
    entityName: 'teu_product_document',
    entity: 'teu_product_document',
    placeholderSnippet: 'teu-document.general.placeholderSearchBar',
    listingRoute: 'teu.document.list'
};
if(swVersion[0] == '6' && swVersion[1] == '3') {
    searchTypeOptions['entityService'] = 'TeuProductDocumentService';
}

Application.addServiceProviderDecorator('searchTypeService', searchTypeService => {
    searchTypeService.upsertType('teu_product_document', searchTypeOptions);
    return searchTypeService;
});




