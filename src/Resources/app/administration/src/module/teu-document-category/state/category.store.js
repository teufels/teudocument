const { Criteria } = Shopware.Data;

export default {
    namespaced: true,

    state() {
        return {
            category: null,
        };
    },

    mutations: {
        setActiveCategory(state, { category }) {
            state.category = category;
        },
        setCategoriesToDelete(state, { categoriesToDelete }) {
            state.categoriesToDelete = categoriesToDelete;
        },
    },

    actions: {
        setActiveCategory({ commit }, payload) {
            commit('setActiveCategory', payload);
        },

        loadActiveCategory({ commit }, { repository, id, apiContext }) {
            const criteria = new Criteria();
            return repository.get(id, apiContext, criteria).then((category) => {
                commit('setActiveCategory', { category });
            });
        }
    }
};
