import template from './teu-document-category-tree-view.html.twig';

const { Component } = Shopware;

Component.register('teu-document-category-tree-list-view', {
    template,

    mixins: [
        'placeholder',
    ],

    props: {
        isLoading: {
            type: Boolean,
            required: true,
            default: false,
        },
    },

    computed: {
        category() {
            return Shopware.State.get('teuDocumentCategory').category;
        }
    },
});
