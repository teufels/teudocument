import template from './teu-document-category-tree-list.html.twig';
import './teu-document-category-tree-list.scss';
const { Component } = Shopware;
const { Criteria } = Shopware.Data;
const { mapState } = Shopware.Component.getComponentHelper();

/**
 * @package content
 */

Component.register('teu-document-category-tree-list', {
    template,

    inject: ['repositoryFactory', 'syncService'],

    mixins: ['notification'],

    props: {
        categoryId: {
            type: String,
            required: false,
            default: null,
        },

        currentLanguageId: {
            type: String,
            required: true,
        },
    },

    data() {
        return {
            loadedCategories: {},
            translationContext: 'teu-product-document-category',
            linkContext: 'teu.document.category.detail.base',
            isLoadingInitialData: true,
            loadedParentIds: [],
            sortable: true,
        };
    },

    computed: {
        ...mapState('teuDocumentCategoryDetail', [
            'categoriesToDelete',
        ]),

        categoryRepository() {
            return this.repositoryFactory.create('teu_product_document_category');
        },

        categories() {
            return Object.values(this.loadedCategories);
        },

        category() {
            return Shopware.State.get('teuDocumentCategory').category;
        },

        disableContextMenu() {

            return this.currentLanguageId !== Shopware.Context.api.systemLanguageId;
        },

        criteria() {
            return new Criteria(1, 500)
        },

        criteriaWithChildren() {
            const parentCriteria = Criteria.fromCriteria(this.criteria).setLimit(1);
            parentCriteria.associations.push({
                association: 'children',
                criteria: Criteria.fromCriteria(this.criteria),
            });

            return parentCriteria;
        },

        documentRepository() {
            return this.repositoryFactory.create('teu_product_document');
        },
    },

    watch: {
        categoriesToDelete(value) {
            if (value === undefined) {
                return;
            }

            this.$refs.categoryTree.onDeleteElements(value);

            Shopware.State.commit('teuDocumentCategory/setCategoriesToDelete', {
                categoriesToDelete: undefined,
            });
        },

        category(newVal, oldVal) {
            // load data when path is available
            if (!oldVal && this.isLoadingInitialData) {
                this.openInitialTree();
                return;
            }

            // back to index
            if (newVal === null) {
                return;
            }

            // reload after save
            if (oldVal && newVal.id === oldVal.id) {
                const affectedCategoryIds = [
                    newVal.id
                ];

                const criteria = Criteria.fromCriteria(this.criteria)
                    .setIds(affectedCategoryIds.filter((value, index, self) => {
                        return value !== null && self.indexOf(value) === index;
                    }));

                this.categoryRepository.search(criteria).then((categories) => {
                    this.addCategories(categories);
                });
            }
        },

        currentLanguageId() {
            this.openInitialTree();
        },
    },

    created() {
        this.createdComponent();
    },

    methods: {
        createdComponent() {
            if (this.category !== null) {
                this.openInitialTree();
            }

            if (!this.categoryId) {
                this.loadRootCategories().finally(() => {
                    this.isLoadingInitialData = false;
                });
            }
        },

        openInitialTree() {
            this.isLoadingInitialData = true;
            this.loadedCategories = {};
            this.loadedParentIds = [];

            this.loadRootCategories()
                .then(() => {
                    if (!this.category || this.category.path === null) {
                        this.isLoadingInitialData = false;
                        return Promise.resolve();
                    }

                    const parentIds = this.category.path.split('|').filter((id) => !!id);
                    const parentPromises = [];

                    parentIds.forEach((id) => {
                        const promise = this.categoryRepository.get(id, Shopware.Context.api, this.criteriaWithChildren)
                            .then((result) => {
                                this.addCategories([result, ...result.children]);
                            });
                        parentPromises.push(promise);
                    });

                    return Promise.all(parentPromises).then(() => {
                        this.isLoadingInitialData = false;
                    });
                });
        },

        onUpdatePositions: Shopware.Utils.debounce(function onUpdatePositions({ draggedItem, oldParentId, newParentId }) {
            if (draggedItem.children.length > 0) {
                draggedItem.children.forEach((child) => {
                    this.removeFromStore(child.id);
                });
                this.loadedParentIds = this.loadedParentIds.filter((id) => id !== draggedItem.id);
            }

            this.syncSiblings({ parentId: newParentId }).then(() => {
                if (oldParentId !== newParentId) {
                    this.syncSiblings({ parentId: oldParentId }).then(() => {
                    });
                }

                this.sortable = true;
            });
        }, 400),




        async deleteCheckedItems(checkedItems) {
            const ids = Object.keys(checkedItems);


            await this.categoryRepository.syncDeleted(ids, Shopware.Context.api);

            const categories = ids.map((id) => this.loadedCategories[id]);

            await this.fixSortingForCategories(categories);

            ids.forEach(id => {
                this.removeFromStore(id);
            });
        },

        onDeleteCategory({ data: category, children, checked }) {
            if (category.isNew()) {
                this.$delete(this.loadedCategories, category.id);
                return Promise.resolve();
            }

            if (this.isErrorNavigationEntryPoint(category)) {
                // remove delete flags
                category.isDeleted = false;
                if (children.length > 0) {
                    children.forEach((child) => {
                        child.data.isDeleted = false;
                    });
                }

                // reinsert category in sorting because the tree
                // already overwrites the afterCategoryId of the following category
                const next = this.getNextCategory(category);

                if (next) {
                    next.afterCategoryId = category.id;
                }

                // reload after changes
                this.loadedCategories = { ...this.loadedCategories };

                return Promise.resolve();
            }

            return this.categoryRepository.delete(category.id).then(async () => {
                this.removeFromStore(category.id);

                if (category.parentId !== null) {
                    const updatedParent = await this.categoryRepository.get(
                        category.parentId,
                        Shopware.Context.api,
                        this.criteria,
                    );
                    this.addCategory(updatedParent);
                }

                await this.fixSortingForCategories([category], true);

                if (category.id === this.categoryId) {
                    this.$router.push({ name: 'teu.document.category.tree' });
                }

            });
        },

        fixSortingForCategories(categories, isSorted = false) {
            const categoriesToBeChanged = [];

            categories.forEach(category => {
                // We need the second parameter, because the value of `afterCategoryId` of the actual next category
                // is either updated already in case of `onDeleteCategory`, but not in case of `deleteCheckedItems`
                const nextCategory = this.getNextCategory(category, isSorted ? 'afterCategoryId' : 'id');

                if (!nextCategory) {
                    return;
                }

                nextCategory.afterCategoryId = category.afterCategoryId;

                if (categories.find(item => item.id === nextCategory.id)) {
                    return;
                }

                categoriesToBeChanged.push(nextCategory);
            });

            return this.categoryRepository.saveAll(categoriesToBeChanged);
        },

        getNextCategory(category, key = 'id') {
            return Object.values(this.loadedCategories).find((item) => {
                return item.parentId === category.parentId && item.afterCategoryId === category[key];
            });
        },

        changeCategory(category) {
            const route = { name: 'teu.document.category.detail', params: { id: category.id } };
            if (this.category && this.categoryRepository.hasChanges(this.category)) {
                this.$emit('unsaved-changes', route);
            } else {
                this.$router.push(route);
            }
        },

        onGetTreeItems(parentId) {
            if (this.loadedParentIds.includes(parentId)) {
                return Promise.resolve();
            }

            this.loadedParentIds.push(parentId);
            const criteria = Criteria.fromCriteria(this.criteria);
            criteria.addFilter(Criteria.equals('parentId', parentId));
            // in case the criteria has been altered to search specific ids e.g. by dragndrop position change
            // reset all ids so categories can be found solely by parentId
            criteria.setIds([]);

            return this.categoryRepository.search(criteria).then((children) => {
                this.addCategories(children);
            }).catch(() => {
                this.loadedParentIds = this.loadedParentIds.filter((id) => {
                    return id !== parentId;
                });
            });
        },

        getChildrenFromParent(parentId) {
            return this.onGetTreeItems(parentId);
        },

        loadRootCategories() {
            const criteria = Criteria.fromCriteria(this.criteria)
                .addFilter(Criteria.equals('parentId', null));

            return this.categoryRepository.search(criteria).then((result) => {
                this.addCategories(result);
            });
        },

        createNewElement(contextItem, parentId, name = '') {
            this.sortable = false;

            if (!parentId && contextItem) {
                parentId = contextItem.parentId;
            }
            const newCategory = this.createNewCategory(name, parentId);
            this.addCategory(newCategory);
            return newCategory;
        },

        createNewCategory(name, parentId) {
            const newCategory = this.categoryRepository.create();

            newCategory.name = name;
            newCategory.parentId = parentId;
            newCategory.childCount = 0;
            newCategory.active = false;
            newCategory.visible = true;

            newCategory.save = () => {
                return this.categoryRepository.save(newCategory).then(() => {
                    const criteria = Criteria.fromCriteria(this.criteria)
                        .setIds([newCategory.id, parentId].filter((id) => id !== null));
                    this.categoryRepository.search(criteria).then((categories) => {
                        this.addCategories(categories);

                        this.sortable = true;
                    });
                });
            };

            return newCategory;
        },

        syncSiblings({ parentId }) {
            const siblings = this.categories.filter((category) => {
                return category.parentId === parentId;
            });

            return this.categoryRepository.sync(siblings).then(() => {
                this.loadedParentIds = this.loadedParentIds.filter(id => id !== parentId);
                return this.getChildrenFromParent(parentId);
            }).then(() => {
                this.categoryRepository.get(parentId, Shopware.Context.api, this.criteria).then((parent) => {
                    this.addCategory(parent);
                });
            });
        },

        addCategory(category) {
            if (!category) {
                return;
            }
            this.loadedCategories = { ...this.loadedCategories, [category.id]: category };
        },

        addCategories(categories) {
            categories.forEach((category) => {
                this.loadedCategories[category.id] = category;
            });
            this.loadedCategories = { ...this.loadedCategories };
        },

        removeFromStore(id) {
            const deletedIds = this.getDeletedIds(id);
            this.loadedParentIds = this.loadedParentIds.filter((loadedId) => {
                return !deletedIds.includes(loadedId);
            });

            deletedIds.forEach((deleted) => {
                this.$delete(this.loadedCategories, deleted);
            });
        },

        getDeletedIds(idToDelete) {
            const idsToDelete = [idToDelete];
            Object.keys(this.loadedCategories).forEach((id) => {
                const currentCategory = this.loadedCategories[id];
                if (currentCategory.parentId === idToDelete) {
                    idsToDelete.push(...this.getDeletedIds(id));
                }
            });
            return idsToDelete;
        },

        getCategoryUrl(category) {
            return this.$router.resolve({
                name: this.linkContext,
                params: { id: category.id },
            }).href;
        },


        isErrorNavigationEntryPoint(category) {
            return false;
        },
    },
});
