import template from './teu-document-category-tree.html.twig';
import categoryState from '../../state/category.store';

const {Component, Mixin} = Shopware;

Component.register('teu-document-category-tree', {
    template,

    inject: [
        'repositoryFactory',
    ],

    mixins: [
        Mixin.getByName('placeholder'),
        Mixin.getByName('notification'),
    ],

    shortcuts: {
        'SYSTEMKEY+S': 'onSave',
        ESCAPE: 'cancelEdit'
    },

    props: {
        categoryId: {
            type: String,
            required: false,
            default: null
        }
    },

    data() {
        return {
            isLoading: false,
            isSaveSuccessful: false,
            nextRoute: null,
            currentLanguageId: Shopware.Context.api.languageId,
            isDisplayingLeavePageWarning: false,
            forceDiscardChanges: false,
        };
    },

    metaInfo() {
        return {
            title: this.$createTitle(),
        };
    },

    computed: {
        showEmptyState() {
            return !this.isLoading && !this.category;
        },

        categoryRepository() {
            return this.repositoryFactory.create('teu_product_document_category');
        },

        category() {
            if (!Shopware.State.get('teuDocumentCategory')) {
                return {};
            }

            return Shopware.State.get('teuDocumentCategory').category;
        },

        tooltipSave() {
            const systemKey = this.$device.getSystemKey();

            return {
                message: `${systemKey} + S`,
                appearance: 'light'
            };
        },

        tooltipCancel() {
            return {
                message: 'ESC',
                appearance: 'light'
            };
        }
    },

    watch: {
        categoryId() {
            this.setCategory();
        },
    },

    beforeCreate() {
        Shopware.State.registerModule('teuDocumentCategory', categoryState);
    },

    created() {
        this.createdComponent();
        this.setCategory();
    },

    beforeDestroy() {
        Shopware.State.unregisterModule('teuDocumentCategory');
    },

    beforeRouteLeave(to, from, next) {
        if (this.forceDiscardChanges) {
            this.forceDiscardChanges = false;
            next();

            return;
        }

        if (this.category && this.categoryRepository.hasChanges(this.category)) {
            this.isDisplayingLeavePageWarning = true;
            this.nextRoute = to;
            next(false);
        } else {
            next();
        }
    },

    methods: {
        createdComponent() {
            this.isLoading = true;
        },

        setCategory() {
            this.isLoading = true;

            if (this.categoryId === null) {
                Shopware.State.commit('shopwareApps/setSelectedIds', []);

                return Shopware.State.dispatch('teuDocumentCategory/loadActiveCategory', {
                    category: null,
                    repository: this.categoryRepository,
                    apiContext: Shopware.Context.api,
                })
                    .then(() => {
                        this.isLoading = false;
                    });
            }

            Shopware.State.commit('shopwareApps/setSelectedIds', [this.categoryId]);
            return Shopware.State.dispatch('teuDocumentCategory/loadActiveCategory', {
                repository: this.categoryRepository,
                apiContext: Shopware.Context.api,
                id: this.categoryId
            })
                .then(() => {
                    this.isLoading = false;
                });
        },


        cancelEdit() {
            this.resetCategory();
        },

        resetCategory() {
            this.$router.push({name: 'teu.document.category.tree'});
        },

        saveFinish() {
            this.isSaveSuccessful = false;
        },

        onSave() {
            this.isSaveSuccessful = false;

            this.isLoading = true;
            if (
                !this.category.name
            ) {
                this.createNotificationError({
                    title: this.$tc('global.default.error'),
                    message: this.$tc(
                        'global.notification.notificationSaveErrorMessageRequiredFieldsInvalid'
                    )
                });
                this.isLoading = false;
            } else {
                this.categoryRepository.save(this.category, Shopware.Context.api).then(() => {
                    return this.setCategory();
                }).then(() => {
                    this.isSaveSuccessful = true;

                    this.createNotificationSuccess({
                        title: this.$tc('global.default.success'),
                        message: this.$tc('teu-document-category.main.messageSaveCategorySuccess', 0, {
                            name: this.category.translated.name
                        })
                    });
                }).catch(() => {
                    this.isLoading = false;
                    this.createNotificationError({
                        message: this.$tc(
                            'global.notification.notificationSaveErrorMessageRequiredFieldsInvalid'
                        )
                    });
                }).finally(() => {
                    this.isLoading = false;
                });
            }
        },

        onRefresh() {
        },

        onCloseModal() {
        },

        onConfirmModal() {
        },

        onChangeLanguage(newLanguageId) {
            this.currentLanguageId = newLanguageId;
            this.setCategory();
        },

        saveOnLanguageChange() {
            return this.onSave();
        },

        abortOnLanguageChange() {
            if (this.category) {
                return this.categoryRepository.hasChanges(this.category);
            }

            return false;
        },

        openChangeModal(destination) {
            this.nextRoute = destination;
            this.isDisplayingLeavePageWarning = true;
        },

        onLeaveModalClose() {
            this.nextRoute = null;
            this.isDisplayingLeavePageWarning = false;
        },

        onLeaveModalConfirm(destination) {
            this.forceDiscardChanges = true;
            this.isDisplayingLeavePageWarning = false;

            this.$nextTick(() => {
                this.$router.push({name: destination.name, params: destination.params});
            });
        },
    }
});
