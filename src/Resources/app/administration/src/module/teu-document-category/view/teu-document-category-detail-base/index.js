import template from './teu-document-category-detail-base.html.twig';

const { Component } = Shopware;
const { Criteria } = Shopware.Data;
const { mapState, mapPropertyErrors } = Shopware.Component.getComponentHelper();


Component.register('teu-document-category-detail-base', {
    template,

    inject: [
        'repositoryFactory',
    ],

    mixins: [
        'placeholder',
    ],

    props: {
        isLoading: {
            type: Boolean,
            required: true,
        }
    },

    data() {
        return {
            customFieldSets: []
        };
    },

    computed: {

        /*...mapState('category', {
            customFieldSets: state => {
                if (!state.customFieldSets) {
                    return [];
                }

                return state.customFieldSets;
            },
        }),*/

        ...mapPropertyErrors('category', [
            'name',
        ]),

        category() {
            return Shopware.State.get('teuDocumentCategory').category;
        },

        customFieldSetRepository() {
            return this.repositoryFactory.create('custom_field_set');
        },

        customFieldSetCriteria() {
            const criteria = new Criteria();
            criteria.addFilter(Criteria.equals('relations.entityName', 'teu_product_document_category'));
            criteria.getAssociation('customFields').addSorting(Criteria.sort('config.customFieldPosition', 'ASC', true));

            return criteria;
        },
    },

    created() {
        this.createdComponent();
    },

    methods: {
        createdComponent() {
            this.getCustomFieldsets();
        },

        getCustomFieldsets() {
            this.customFieldSetRepository.search(this.customFieldSetCriteria, Shopware.Context.api)
                .then((customFieldSets) => {
                    this.customFieldSets = customFieldSets;
                });
        }
    }
});
