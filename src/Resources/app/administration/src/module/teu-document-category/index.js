import './page/teu-document-category-tree';
import './component/teu-document-category-tree-list'
import './component/teu-document-category-tree-view'
import './view/teu-document-category-detail-base'
import deDE from './snippet/de-DE';
import enGB from './snippet/en-GB';

Shopware.Module.register('teu-document-category', {
    type: 'plugin',
    name: 'DocumentCategory',
    title: 'teu-document-category.main.menuLabel',
    description: 'teu-document-category.main.menuDescription',
    color: '#000',
    icon: 'regular-layer-group',
    entity: 'teu_product_document_category',
    entityDisplayProperty: 'name',

    snippets: {
        'de-DE': deDE,
        'en-GB': enGB
    },

    routes: {
        index: {
            component: 'teu-document-category-tree',
            path: 'index',
            meta: {
                parentPath: 'teu.document.category.index',
            },
        },
        detail: {
            component: 'teu-document-category-tree',
            path: 'index/:id',
            redirect: {
                name: 'teu.document.category.detail.base',
            },
            children: {
                base: {
                    component: 'teu-document-category-detail-base',
                    path: 'base',
                    meta: {
                        parentPath: 'teu.document.category.index',
                    },
                }
            },

            props: {
                default(route) {
                    return {
                        categoryId: route.params.id,
                    };
                },
            },
        }

    },

    navigation: [{
        label: 'teu-document-category.main.menuLabel',
        color: '#000',
        path: 'teu.document.category.index',
        icon: 'regular-layer-group',
        parent: 'sw-catalogue',
        position: 101
    }],

});
